var express = require('express');
const router = express.Router();
const moment = require('moment');
const csv = require('csvtojson')
const request = require('request')
const fs = require('fs')
const axios = require('axios')
const schedule = require('node-schedule');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const SITE = ['GLOBAL', 'THAILAND', 'VIETNAM', 'LAOS', 'CAMBODIA']


router.get('/site', async function(req, res, next) {
    const query = new Parse.Query('SiteChannel')
    query.limit(9999)
    query.select(['name'])
    const channel = await query.find({ useMasterKey: true })
    res.send({ status: 'success', data: channel.map(item => item.get('name')) })
})

router.post('/site', async function(req, res, next) {
    const { name } = req.body
    const SiteChannel = Parse.Object.extend('SiteChannel');
    const query = new Parse.Query('SiteChannel')
    query.limit(9999)
    query.equalTo('name', name)
    const channel = await query.first({ useMasterKey: true })
    if (channel || SITE.includes(name)) {
        res.send({ status: 'success' })
    } else {
        const obj = new SiteChannel()
        await obj.save({ name }, { useMasterKey: true })
        res.send({ status: 'success' })
    }
})

router.get('/dataTypes', async function(req, res, next) {
    const datatypeQuery = new Parse.Query('DataType')
    datatypeQuery.limit(9999)
    const dataTypeObjs = await datatypeQuery.find()
    const dataTypes = dataTypeObjs.map(item => item.toJSON())
        //map data  Types to TreeSelect Format
    let rawTypeData = dataTypes.map(data => {
        return {
            name: data.name,
            title: data.name,
            value: data.objectId,
            parentId: data.ParentId,
            objectId: data.objectId,
        }
    });
    let data = rawTypeData.filter(data => data.parentId === undefined);
    rawTypeData.map(item => {
        const CurrentItem = rawTypeData.filter(data => data.parentId === item.objectId)
        item.children = CurrentItem
        return item
    })
    res.send({ status: 'success', data })
});

//Query Layers List With Layer Data Files
router.get('/layers', async function(req, res, next) {
    const layersQuery = new Parse.Query('LayerList')
    layersQuery.limit(9999)
    const layersobjs = await layersQuery.find()
    const layersList = layersobjs.map(item => { return {...item.toJSON(), children: [] } })
    const layerData = await getLayerDataWithId();
    const rawLayersData = layersList.map(item => {
        const currentItem = layerData.data.filter(data => data.layerId === item.objectId).map(item => {
            const { fileUrl, legendUrl, north, east, south, west, fileDate } = item
            item.border = [
                [south, west],
                [north, east]
            ]
            item.textureUrl = fileUrl
            item.legendUrl = legendUrl
            item.date = moment(fileDate.iso).format('YYYY-MM-DD')
            return item
        })
        item.children = currentItem
        return item
    })
    res.send({ status: 'success', data: rawLayersData })
});

//Query Layers List With Layer Data Files
router.get('/stations', async function(req, res, next) {
    const { stationId, typeId } = req.query
    const stationQuery = new Parse.Query('Station')
    if (stationId) {
        stationQuery.equalTo('objectId', stationId)
    }
    stationQuery.limit(9999)
    const stationobjs = await stationQuery.find()
    console.log('stationobjs', stationobjs);
    const stationList = stationobjs.map(item => { return {...item.toJSON(), children: [] } })
    const stationData = await getStationData({ stationId, typeId });
    console.log('stationData', stationData);
    const rawStationData = []
    for (const item of stationList) {
        //const dailyHydroMet = await getCurrentDailyHydroData({ stationCode: item.stationCode })
        const filtered = stationData.data.filter(data => data.stationId === item.objectId)
        for (const data of filtered) {
            // const { typeName } = data

            // const processHydro = await getProcessHydroData({ stationCode: item.stationCode, type: typeName === 'Rainfall' ? 'RainFall' : typeName })
            // const timeSeriesDataList = processHydro.data ? processHydro.data.map(item => {
            //     return { date: item.dateStr, value: item.value }
            // }) : []

            // if (dailyHydroMet['data']) {
            //     if (typeName === 'Rainfall') {
            //         timeSeriesDataList.push({ date: moment(dailyHydroMet['data']['lastMeasurement']).format('YYYY-MM-DD'), value: parseFloat(dailyHydroMet['data']['rainFall'], 0.0) })
            //     } else if (typeName === 'WaterLevel') {
            //         timeSeriesDataList.push({ date: moment(dailyHydroMet['data']['lastMeasurement']).format('YYYY-MM-DD'), value: parseFloat(dailyHydroMet['data']['waterLevel'], 0.0) })
            //     }
            // }

            //data.timeSeriesData = timeSeriesDataList
            data.position = [item.lat, item.lng]
            data.stationCode = item.stationCode
            rawStationData.push(data)
        }
    }
    res.send({ status: 'success', data: rawStationData })
});

router.get('/hydroData', async function(req, res, next) {
    const { stationCode, typeName } = req.query
    const dailyHydroMet = await getCurrentDailyHydroData({ stationCode })
    const processHydro = await getProcessHydroData({ stationCode, type: typeName === 'Rainfall' ? 'RainFall' : typeName })
    const timeSeriesDataList = processHydro.data ? processHydro.data.map(item => {
        return { date: item.dateStr, value: item.value }
    }) : []
    if (dailyHydroMet['data']) {
        if (typeName === 'Rainfall') {
            timeSeriesDataList.push({ date: moment(dailyHydroMet['data']['lastMeasurement']).format('YYYY-MM-DD'), value: parseFloat(dailyHydroMet['data']['rainFall'], 0.0) })
        } else if (typeName === 'WaterLevel') {
            timeSeriesDataList.push({ date: moment(dailyHydroMet['data']['lastMeasurement']).format('YYYY-MM-DD'), value: parseFloat(dailyHydroMet['data']['waterLevel'], 0.0) })
        }
    }
    const sorted = timeSeriesDataList.sort((a, b) => a.date.localeCompare(b.date))
    res.send({ status: 'success', data: sorted })
});

router.get('/csvData/:dataId', async function(req, res, next) {
    const { dataId } = req.params
    const query = new Parse.Query('StationData');
    query.limit(9999)
    const data = await query.get(dataId, { useMasterKey: true })
    if (!data) {
        res.send({ status: 'error', message: 'Not found data' })
    }
    const { fileUrl, name, typeName } = data.toJSON()
    const check = fs.existsSync(`files/csv/${name}.csv`)
    console.log(`${name}:${fileUrl} ll ${check}`);
    let timeSeriesData = ''
    if (!check) {
        if (fileUrl) {
            const readBuffer = await axios.get(fileUrl, {
                responseType: 'arraybuffer',
            })
            fs.writeFileSync(`files/csv/${name}.csv`, readBuffer.data, { encoding: 'base64' }) // utf8 will override this encoding
            timeSeriesData = readBuffer.data.toString();
        }
    } else {
        const buffer = fs.readFileSync(`files/csv/${name}.csv`)
        timeSeriesData = buffer.toString();
    }
    let rawData = await csv().fromString(timeSeriesData)
    const csvParsedData = rawData.map(item => {
        // if (typeName === 'Rainfall') {
        //     item.Date = moment(item.Date, 'DD/MM/YYYY').format('YYYY-MM-DD')
        // }
        return { date: item.Date, value: parseFloat(item[typeName], 0.0) }
    })
    res.send({ status: 'success', data: csvParsedData })

});


router.get('/csv/:name', async function(req, res, next) {
    const { name } = req.params
    const splitName = name.replace('.csv', '')
    const stationData = await getStationData({ name: splitName });
    if (stationData.status === 'success') {
        const { fileUrl, name, typeName } = stationData.data[0]
        console.log('name', name);
        const buffer = fs.readFileSync(`files/csv/${name}.csv`)
        let timeSeriesData
        if (!buffer) {
            await request.get(fileUrl).pipe(fs.createWriteStream(`files/csv/${name}.csv`))
            const buffer2 = fs.readFileSync(`files/csv/${name}.csv`)
            timeSeriesData = buffer2.toString();
        } else {
            timeSeriesData = buffer.toString();
        }
        let rawData = await csv().fromString(timeSeriesData)
        const data = rawData.map(item => {
            // if (typeName === 'Rainfall') {
            //     item.Date = moment(item.Date, 'DD/MM/YYYY').format('YYYY-MM-DD')
            // }
            return { date: item.Date, value: parseFloat(item[typeName], 0.0) }
        })
        res.send({ status: 'success', data })
    } else {
        res.send({ status: 'error', message: 'no files found' })
    }

});


//Query LayerData 
router.get('/layerData', async function(req, res, next) {
    const { countryId, areaId, typeId } = req.query;
    const Content = new Parse.Object('LayerData');
    const query = new Parse.Query(Content);
    if (countryId) {
        query.equalTo('countryId', countryId)
    }
    if (areaId) {
        query.equalTo('areaId', areaId)
    }
    if (typeId) {
        query.equalTo('typeId', typeId)
    }
    query.limit(99999)
    query.descending('createdAt')
    const objs = await query.find({ useMasterKey: true });
    if (objs) {
        const data = objs.map(item => item.toJSON())
        res.send({ status: 'success', data })
    } else {
        res.send({ status: 'error', error: 'Not found Content' })
    }
});

//Query StationData 
//Parameter: countryId, areaId, typeId
//example: http://localhost:1337/api/v1/data/stationData/[countryId]/[areaId]/[typeId]
router.get('/stationData', async function(req, res, next) {
    const { countryId, areaId, typeId } = req.query;
    const Content = new Parse.Object('StationData');
    const query = new Parse.Query(Content);
    if (countryId) {
        query.equalTo('countryId', countryId)
    }
    if (areaId) {
        query.equalTo('areaId', areaId)
    }
    if (typeId) {
        query.equalTo('typeId', typeId)
    }
    query.descending('createdAt')
    query.limit(9999)
    const objs = await query.find({ useMasterKey: true });
    if (objs) {
        const data = objs.map(item => item.toJSON())
        res.send({ status: 'success', data })
    } else {
        res.send({ status: 'error', error: 'Not found Content' })
    }
});

router.post('/rainfall', async function(req, res, next) {
    const { list } = req.body;
    const Rainfall = Parse.Object.extend("Rainfall");
    const mapped = list.map(item => {
        const obj = new Rainfall();
        delete item.id
        console.log('ite,', item);
        return obj.save(item, { useMasterKey: true })
    })
    const resss = await Promise.all(mapped)
    console.log('resss', resss);
    res.send({ status: 'success' });
});

router.get('/location', async function(req, res, next) {
    const countryQuery = new Parse.Query('Country')
    countryQuery.limit(9999)
    const countryObjs = await countryQuery.find()
    const countries = countryObjs.map(item => item.toJSON())
    console.log('get dataTypes', countries);

    const AreaQuery = new Parse.Query('Area')
    const areaObjs = await AreaQuery.find()
    const areas = areaObjs.map(item => item.toJSON())
    console.log('get location areas', areas);

    let temp = []
    areas.forEach(item => {
        item.children = areas.filter(data => data.parentID === item.objectId)
        if (item.children.length === 0) {
            delete item.children
        }
        item.title = item.name
        item.value = 'area-' + item.objectId
        temp.push(item)
    })
    temp = temp.filter(item => !item.parentID)
    const data = countries.map(contry => {
        contry.children = temp.filter(item => item.countryId === contry.objectId)
        contry.title = contry.name
        contry.value = 'country-' + contry.objectId
        return contry
    })
    res.send({ status: 'success', data })
});

const getLayerDataWithId = async function(data) {
    const { layerId } = data || {};
    const Content = new Parse.Object('LayerData');
    const query = new Parse.Query(Content);
    if (layerId) {
        query.equalTo('layerId', layerId)
    }
    query.limit(99999)
    const objs = await query.find({ useMasterKey: true });
    if (objs) {
        const data = objs.map(item => item.toJSON())
        return { status: 'success', data };
    } else {
        return { status: 'error', error: 'Not found Content' }
    }
}

const getStationData = async function(data) {
    const { stationId, name, typeId } = data || {};
    const query = new Parse.Query('StationData');
    if (stationId) {
        query.equalTo('stationId', stationId)
    }
    if (typeId) {
        query.equalTo('typeId', typeId)
    }
    if (name) {
        query.equalTo('name', name)
    }
    query.limit(99999)
    const objs = await query.find({ useMasterKey: true });
    if (objs) {
        const data = objs.map(item => item.toJSON())
        return { status: 'success', data };
    } else {
        return { status: 'error', error: 'Not found Content' }
    }
}

const getDailyHydroData = async function(data) {
    const { stationCode } = data || {};
    const query = new Parse.Query('DailyHydroMet');
    if (stationCode) {
        query.equalTo('stationCode', stationCode)
    }
    query.greaterThanOrEqualTo('createdAt', moment().startOf('days').toDate())
    query.lessThan('createdAt', moment().endOf('days').toDate())
    query.limit(99999)
    query.skip(0)
    query.descending('createdAt')
    const objs = await query.find({ useMasterKey: true });
    if (objs) {
        const data = objs.map(item => item.toJSON())
        return { status: 'success', data };
    } else {
        return { status: 'error', error: 'Not found Content' }
    }
}

const getProcessHydroData = async function(data) {
    const { stationCode, type } = data || {};
    const query = new Parse.Query('ProcessHydroMet');
    if (stationCode) {
        query.equalTo('stationCode', stationCode)
    }
    if (type) {
        query.equalTo('type', type)
    }
    query.lessThan('date', moment().startOf('days').toDate())
    query.limit(99999)
    query.skip(0)
    query.descending('date')
    const objs = await query.find({ useMasterKey: true });
    if (objs) {
        const data = objs.map(item => item.toJSON())
        return { status: 'success', data };
    } else {
        return { status: 'error', error: 'Not found Content' }
    }
}

const getCurrentDailyHydroData = async function(data) {
    const { stationCode } = data || {};
    const query = new Parse.Query('DailyHydroMet');
    if (stationCode) {
        query.equalTo('stationCode', stationCode)
    }
    query.greaterThanOrEqualTo('createdAt', moment().startOf('days').toDate())
    query.lessThan('createdAt', moment().endOf('days').toDate())
    query.limit(99999)
    query.skip(0)
    query.descending('createdAt')
    const objs = await query.first({ useMasterKey: true });
    if (objs) {
        return { status: 'success', data: objs.toJSON() };
    } else {
        return { status: 'error', error: 'Not found Content' }
    }
}

router.get('/rainfallTMD', async function(req, res, next) {
    const response = await axios.get('https://data.tmd.go.th/api/Weather3Hours/V2/?uid=api&ukey=api12345');

    if (response.data) {
        //var parser = new JSDOM();
        var xmlDoc = new JSDOM(response.data, "text/xml").window.document;
        var stations = xmlDoc.querySelectorAll("Station");

        let stationTMDvalue = [];
        for (var i = 0; i < stations.length; i++) {
            var stationNameThai = stations[i].querySelector("StationNameThai").textContent;
            var stationNameEnglish = stations[i].querySelector("StationNameEnglish").textContent;
            var latitudeValue = stations[i].querySelector("Latitude").textContent;
            var longitudeValue = stations[i].querySelector("Longitude").textContent;

            var observation = stations[i].querySelector("Observation");
            var dateTime = observation.querySelector("DateTime").textContent;
            var rainfall24Hr = observation.querySelector("Rainfall24Hr").textContent;

            stationTMDvalue.push({
                stationNameThai: stationNameThai,
                stationNameEnglish: stationNameEnglish,
                latitudeValue: latitudeValue,
                longitudeValue: longitudeValue,
                dateTime: dateTime,
                rainfall24Hr: rainfall24Hr
            })
        }

        const data = stationTMDvalue.filter(function(item) {
            return true;
        }).map(function(item) {
            return {
                name: item.stationNameEnglish || item.stationNameThai || "undefined",
                position: [
                    item.latitudeValue, item.longitudeValue
                ],
                Rain: item.rainfall24Hr,
                Timestamp: item.dateTime,
                ...item
            }
        });
        res.send({ status: 'success', data: data });
    } else {
        res.send({ status: 'failed', data: [] });
    }
});

router.get('/nrth', async(req, res, next) => {
    const resp = await axios.get('https://api.mrcmekong.org/api/v1/nrtm/station')
    if (resp.data) {
        const data = resp
            .data
            .map(item => {
                return {
                    id: item.stationId,
                    position: [
                        item.latitude, item.longitude
                    ],
                    ...item
                }
            })
        res.send(data)
    } else {
        res.send([])
    }
})


const INTERVAL = '0 */15 * * * *'

schedule.scheduleJob(INTERVAL, async() => {
    console.log('start crawling data...');
    const resp = await axios.get('https://api.mrcmekong.org/api/v1/nrtm/station')
    if (resp.data) {
        const data = resp
            .data
            .map(item => {
                return {
                    stationCode: item.stationId,
                    position: [
                        item.latitude, item.longitude
                    ],
                    ...item
                }
            })
        fs.writeFileSync('files/json/hydro_nrt.json', JSON.stringify(data));
        console.log('test crawled data index 0:', data[0]);
        const HydroMetData = Parse.Object.extend("DailyHydroMet");
        const mapped = data.map(item => {
            const obj = new HydroMetData();
            return obj.save(item, { useMasterKey: true })
        })
        const saveRes = await Promise.all(mapped)
        console.log('saveRes', saveRes[0]);
    }
});

const midnight = 0
schedule.scheduleJob(`0 1 ${midnight} * * *`, async() => {
    console.log('start process crawled data...');
    const query = new Parse.Query('DailyHydroMet')
    query.greaterThanOrEqualTo('createdAt', moment().startOf('days').toDate())
    query.lessThan('createdAt', moment().endOf('days').toDate())
    query.limit(99999)
    query.skip(0)
    const res = await query.find({ useMasterKey: true })
    const stationKeys_waterLevel = {}
    const stationKeys_rainFall = {}

    res.forEach(item => {
        const data = item.toJSON()
        const { stationCode, waterLevel, rainFall } = data
        if (!stationKeys_waterLevel[stationCode]) {
            stationKeys_waterLevel[stationCode] = waterLevel || 0
        }
        if (stationKeys_waterLevel[stationCode] <= waterLevel) {
            stationKeys_waterLevel[stationCode] = waterLevel
        }
        if (!stationKeys_rainFall[stationCode]) {
            stationKeys_rainFall[stationCode] = rainFall || 0
        }
        if (stationKeys_rainFall[stationCode] <= rainFall) {
            stationKeys_rainFall[stationCode] = rainFall
        }
    })
    const date = moment().startOf('day')
    const waterLevelList = Object.keys(stationKeys_waterLevel).map(item => {
        return {
            stationCode: item,
            value: stationKeys_waterLevel[item],
            type: 'WaterLevel',
            date: date.toDate(),
            dateStr: date.format('YYYY-MM-DD')
        }
    })
    console.log('waterLevelList', waterLevelList);

    const rainFallList = Object.keys(stationKeys_rainFall).map(item => {
        return {
            stationCode: item,
            value: stationKeys_rainFall[item],
            type: 'RainFall',
            date: date.toDate(),
            dateStr: date.format('YYYY-MM-DD')
        }
    })
    console.log('rainFallList', rainFallList);

    const list = [...waterLevelList, ...rainFallList]
    const HydroMetData = Parse.Object.extend("ProcessHydroMet");
    const mapped = list.map(item => {
        const obj = new HydroMetData();
        return obj.save(item, { useMasterKey: true })
    })
    const saveRes = await Promise.all(mapped)
    console.log('save res', saveRes[0]);

});




module.exports = router


// schedule.scheduleJob(INTERVAL, async() => {
//     const resp = await axios.get('https://api.mrcmekong.org/v1/time-series/rainfall/recent')
//     if (resp.data) {
//         const data = resp
//             .data
//             .features
//             .map(item => {
//                 return {
//                     stationCode: item.properties.stationId,
//                     position: [
//                         item.geometry.coordinates[1], item.geometry.coordinates[0]
//                     ],
//                     ...item.properties
//                 }
//             })
//         fs.writeFileSync('files/json/rainfall_nrt.json', JSON.stringify(data));

//     }
// });